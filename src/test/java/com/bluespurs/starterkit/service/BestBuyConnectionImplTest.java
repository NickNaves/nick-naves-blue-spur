package com.bluespurs.starterkit.service;

import static org.junit.Assert.assertTrue;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.bluespurs.starterkit.UnitTest;
import com.bluespurs.starterkit.domain.BestBuyProduct;
import com.bluespurs.starterkit.domain.WalmartProduct;

public class BestBuyConnectionImplTest extends UnitTest {

	
	private BestBuyConnectionImpl svc;
	
	@Before
	public  void setup(){
		svc = new BestBuyConnectionImpl();
	}
	
	@Test
	public void QueryNameDefault() throws ServiceException{		
		
		List<BestBuyProduct> products = svc.queryName("Ipad mini");
		assertTrue(products.size() > 0);
		
	}
	
	@Test(expected = ServiceException.class)
	public void QueryNameWithEmptyString() throws ServiceException{	
		svc.queryName("");		
	}
	
	@Test(expected = ServiceException.class)
	public void QueryNameWithNull() throws ServiceException{	
		svc.queryName(null);	
	}
	
	@Test 
	public void queryLowestPrice() throws ServiceException{
		BestBuyProduct product = svc.queryLowestPrice("Ipad Mini");
		assertTrue(product != null);
	}
}

package com.bluespurs.starterkit.service;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

import java.util.List;

import com.bluespurs.starterkit.UnitTest;
import com.bluespurs.starterkit.domain.WalmartProduct;
import com.bluespurs.starterkit.service.ServiceException;
import com.bluespurs.starterkit.service.WalmartConnectionImpl;

public class WalmartConnectionImplTest extends UnitTest {

	
	private WalmartConnectionImpl svc;
	
	
	@Before
	public  void setup(){
		svc = new WalmartConnectionImpl();
	}
	
	@Test
	public void QueryNameDefault() throws ServiceException{		
		
		List<WalmartProduct> products = svc.queryName("Ipad mini");
		assertTrue(products.size() > 0);
		
	}
	
	@Test(expected = ServiceException.class)
	public void QueryNameWithEmptyString() throws ServiceException{	
		svc.queryName("");		
	}
	
	@Test(expected = ServiceException.class)
	public void QueryNameWithNull() throws ServiceException{	
		svc.queryName(null);	
	}
	
	@Test 
	public void queryLowestPrice() throws ServiceException{
		WalmartProduct product = svc.queryLowestPrice("Ipad Mini");
		assertTrue(product != null);
	}
}

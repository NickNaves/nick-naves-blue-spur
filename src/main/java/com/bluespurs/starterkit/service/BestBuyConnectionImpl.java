package com.bluespurs.starterkit.service;

import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.bluespurs.starterkit.domain.*;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Service
public class BestBuyConnectionImpl implements ConnectionService<BestBuyProduct> {
	
	private static final Logger LOG = LoggerFactory.getLogger(BestBuyConnectionImpl.class);
	private static String URL = "https://api.bestbuy.com/v1/products((";
	private static String FormatAndKey = "))?apiKey=pfe9fpy68yg28hvvma49sc89&format=json";
	
	@Override
	public BestBuyProduct queryLowestPrice(String name) throws ServiceException {
		
		List<BestBuyProduct> products = queryName(name);
		BestBuyProduct lowestPricedProduct = new BestBuyProduct();
		
		if(products.size() > 2){
			lowestPricedProduct = products.get(0);
			for(int x = 1; x < products.size(); x++){					
				if(products.get(x).getPrice() < lowestPricedProduct.getPrice()){
					lowestPricedProduct = products.get(x);
				}
			}
		}
		else if(products.size() > 0){
			lowestPricedProduct = products.get(0);
		}
		else{
			LOG.info("No products matching {}. Returning empty object.", name);
		}
	
		return lowestPricedProduct;
	}
	
	@Override
	public List<BestBuyProduct> queryName(String name) throws ServiceException {
		
		if(name == null || name.trim() == ""){
			LOG.error("Product name for query is null or empty string");
			throw new ServiceException("Name is null or empty");
		}
		
		String url = getFormattedURLString(name);
		RestTemplate restTemplate = new RestTemplate();
		
		List<BestBuyProduct> products = new ArrayList<BestBuyProduct>();
		
		BestBuyQuery query = restTemplate.getForObject(url, BestBuyQuery.class);
			
		if(!query.getProducts().isEmpty()){
			products = query.getProducts();
		}
		
		return products;
	}

	
	private String getFormattedURLString(String name) throws ServiceException{
			
		String query = "name=\"" + name.trim() + "*\"";
		String url = URL + query + FormatAndKey ;
					
		return url;
	}


	
}

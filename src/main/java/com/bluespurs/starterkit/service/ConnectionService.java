package com.bluespurs.starterkit.service;

import java.util.List;

public interface ConnectionService<T> {

	//TODO: Possibly turn interface into base class
	
	public T queryLowestPrice(String name) throws ServiceException;	

	public List<T> queryName(String name) throws ServiceException;

}

package com.bluespurs.starterkit.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bluespurs.starterkit.domain.BestBuyProduct;
import com.bluespurs.starterkit.domain.StoreProduct;
import com.bluespurs.starterkit.domain.WalmartProduct;
import com.bluespurs.starterkit.models.Product;

@Service
public class ProductServiceImpl implements ProductService {

	public static final Logger LOG = LoggerFactory.getLogger(ProductServiceImpl.class);
	
	@Autowired
	BestBuyConnectionImpl bestBuyConn;
	
	@Autowired
	WalmartConnectionImpl walmartConn;
	
	public Product getLowestPrice(String name) throws ServiceException{
		
		Product out;
		
		WalmartProduct walmartProduct = walmartConn.queryLowestPrice(name);
		BestBuyProduct bestBuyProduct = bestBuyConn.queryLowestPrice(name);
		
		
		LOG.debug("Walmart Price: {}", walmartProduct.getPrice());
		LOG.debug("BestBuy Price: {}", bestBuyProduct.getPrice());
		
		if(walmartProduct.compareTo(bestBuyProduct) < 0){
			out = createPOSTProduct(walmartProduct);
		}
		else{
			out = createPOSTProduct(bestBuyProduct);
		}
					
		return out;
	}
	
	private Product createPOSTProduct(StoreProduct prdt){
		
		Product product = new Product();
		
		product.setLocation(prdt.getLocation());
		product.setCurrency("USD"); //NO APIs for Canada
		product.setProductName(prdt.getName());
		product.setBestPrice(prdt.getPrice().toString());
		
		LOG.debug("Product Created: {} ", product);
		
		return product;	
	}	
}

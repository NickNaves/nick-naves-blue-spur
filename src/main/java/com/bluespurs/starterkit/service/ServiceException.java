package com.bluespurs.starterkit.service;

public class ServiceException extends Exception{
 
	//TODO: Clean up entire exception handling
	
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public ServiceException(String message){
        super(message);
    }
 
    public ServiceException(String message, Throwable cause) {
        super(message, cause);
    }
 
}

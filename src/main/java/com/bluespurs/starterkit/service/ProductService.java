package com.bluespurs.starterkit.service;

import com.bluespurs.starterkit.models.Product;

public interface ProductService {

	public Product getLowestPrice(String name) throws ServiceException;
	
}

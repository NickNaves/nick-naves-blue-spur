package com.bluespurs.starterkit.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.springframework.web.client.RestTemplate;

import com.bluespurs.starterkit.domain.WalmartProduct;
import com.bluespurs.starterkit.domain.WalmartQuery;

@Service
public class WalmartConnectionImpl implements ConnectionService<WalmartProduct> {
	
	private static final Logger LOG = LoggerFactory.getLogger(WalmartConnectionImpl.class);
	private static String URL = "http://api.walmartlabs.com/v1/search?query=";
	private static String FormatAndKey = "&format=json&apiKey=rm25tyum3p9jm9x9x7zxshfa";
	
	
	public WalmartProduct queryLowestPrice(String name) throws ServiceException{	
		List<WalmartProduct> products = this.queryName(name);
		WalmartProduct lowestPricedProduct = new WalmartProduct();
		
		if(products.size() > 2){
			lowestPricedProduct = products.get(0);
			for(int x = 1; x < products.size(); x++){					
				if(products.get(x).getPrice() < lowestPricedProduct.getPrice()){
					lowestPricedProduct = products.get(x);
				}
			}
		}
		else if(products.size() > 0){
			lowestPricedProduct = products.get(0);
		}
		else{
			LOG.info("No products matching {}. Returning empty object.", name);
		}
	
		return lowestPricedProduct;
	}
	
	
	@Override
	public List<WalmartProduct> queryName(String name) throws ServiceException {
			
		if(name == null || name.trim() == ""){
			LOG.error("Product name for query is null or empty string");
			throw new ServiceException("Name is null or empty");
		}
		
		String url = getFormattedURLString(name);
		RestTemplate restTemplate = new RestTemplate();
		
		List<WalmartProduct> products = new ArrayList<WalmartProduct>();
		
		WalmartQuery query = restTemplate.getForObject(url, WalmartQuery.class);
			
		if(query.getNumItems() > 0){
			products = query.getItems();
		}
		
		return products;
	}
	
	
	private String getFormattedURLString(String name) throws ServiceException{
				
		String url = "";
		
		try {
			url = URL + URLEncoder.encode(name,"UTF-8") + FormatAndKey ;
		}
		catch(UnsupportedEncodingException ex){
			LOG.error("Exception {} in getFormattedURLString with msg: {}", ex.getClass(), ex.getMessage());
			throw new ServiceException("Error encoding query URL", ex);
		}
		catch(Exception ex){
			LOG.error("Unknown Exception in getFormattedURLString with msg: {}", ex.getMessage());
			throw new ServiceException("Unknown Error encoding query URL");
		}
			
		return url;
	}
	

}

package com.bluespurs.starterkit.controller;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.bluespurs.starterkit.models.Product;
import com.bluespurs.starterkit.models.ProductRequest;
import com.bluespurs.starterkit.service.ProductService;
import com.bluespurs.starterkit.service.ServiceException;


@RestController
public class ProductController  {

	
	//TODO: Create Error handling Controller
	
	public static final Logger log = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	ProductService svc;
	
	@RequestMapping(value="/product/search", method=RequestMethod.GET, produces = "application/json")
	public @ResponseBody Product search(@Valid ProductRequest request){
		
		String name = request.getName();
		
		try{
			Product product = svc.getLowestPrice(name);
			return product;
		}
		catch(ServiceException ex){
			log.error("Service Exception with message: {}", ex.getMessage());
		}
		catch(Exception ex){
			log.error("Unknown Exception with message: {}", ex.getMessage());
		}
		
	    return null;		
	}	
}

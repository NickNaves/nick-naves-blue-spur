package com.bluespurs.starterkit.models;

public class Product {

	private String productName;
	private String bestPrice;
	private String currency;
	private String location;
	
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getBestPrice() {
		return bestPrice;
	}
	public void setBestPrice(String bestPrice) {
		this.bestPrice = bestPrice;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	
	@Override
	public String toString() {
		return "Product [productName=" + productName + ", bestPrice=" + bestPrice + ", currency=" + currency
				+ ", location=" + location + "]";
	}
	
}

package com.bluespurs.starterkit.models;

import org.hibernate.validator.constraints.NotBlank;

public class ProductRequest {

	@NotBlank
	private String name;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
}

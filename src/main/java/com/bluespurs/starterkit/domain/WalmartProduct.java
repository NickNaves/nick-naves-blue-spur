package com.bluespurs.starterkit.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WalmartProduct extends StoreProduct{

	private int itemId;
	
	private Float msrp;
	private Float salePrice;
	private String upc;
	private String shortDescription;
	
	public WalmartProduct(){
		this.setLocation("Walmart");
	}
	
	public int getItemId() {
		return itemId;
	}
	public void setItemId(int itemId) {
		this.itemId = itemId;
	}

	public Float getMsrp() {
		return msrp;
	}
	public void setMsrp(Float msrp) {
		this.msrp = msrp;
	}
	public Float getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(Float salePrice) {
		this.salePrice = salePrice;
	}
	public String getUpc() {
		return upc;
	}
	public void setUpc(String upc) {
		this.upc = upc;
	}
	public String getShortDescription() {
		return shortDescription;
	}
	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}
	
	public Float getPrice(){	
		return (salePrice != null)? salePrice : msrp;	
	}
	
}

package com.bluespurs.starterkit.domain;

import java.util.List;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BestBuyQuery {

	private long total;
	private List<BestBuyProduct> products;
	
	public long getTotal() {
		return total;
	}
	public void setTotal(long total) {
		this.total = total;
	}
	public List<BestBuyProduct> getProducts() {
		return products;
	}
	public void setProducts(List<BestBuyProduct> products) {
		this.products = products;
	}
	
}

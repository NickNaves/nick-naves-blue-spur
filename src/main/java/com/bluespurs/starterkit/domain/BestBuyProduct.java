package com.bluespurs.starterkit.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class BestBuyProduct extends StoreProduct{
	
	private long productId;
	private long sku;
	private Float regularPrice;
	private Float salePrice;
	
	public BestBuyProduct(){
		this.setLocation("BestBuy");
	}
		
	public long getProductId() {
		return productId;
	}
	public void setProductId(long productId) {
		this.productId = productId;
	}
	public long getSku() {
		return sku;
	}
	public void setSku(long sku) {
		this.sku = sku;
	}
	public Float getRegularPrice() {
		return regularPrice;
	}
	public void setRegularPrice(Float regularPrice) {
		this.regularPrice = regularPrice;
	}
	public Float getSalePrice() {
		return salePrice;
	}
	public void setSalePrice(Float salePrice) {
		this.salePrice = salePrice;
	}
	
	public Float getPrice(){	
		return (salePrice != null)? salePrice : regularPrice;
	}

}

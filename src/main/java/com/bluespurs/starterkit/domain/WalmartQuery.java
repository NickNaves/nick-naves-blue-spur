package com.bluespurs.starterkit.domain;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties(ignoreUnknown = true)
public class WalmartQuery {

	private String query;
	private int totalResults;
	private int numItems;
	private List<WalmartProduct> items;
	
	public List<WalmartProduct> getItems() {
		return items;
	}
	public void setItems(List<WalmartProduct> items) {
		this.items = items;
	}
	public int getNumItems() {
		return numItems;
	}
	public void setNumItems(int numItems) {
		this.numItems = numItems;
	}
	public int getTotalResults() {
		return totalResults;
	}
	public void setTotalResults(int totalResults) {
		this.totalResults = totalResults;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
		
}

package com.bluespurs.starterkit.domain;

public abstract class StoreProduct implements Comparable<StoreProduct>{

	private String location;
	private String name;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}	
	
	public abstract Float getPrice();
	
	public int compareTo(final StoreProduct x){
		return Float.compare(this.getPrice(), x.getPrice());
	}
	
}
